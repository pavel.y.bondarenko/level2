package com.bondarenko;

import com.bondarenko.jms.MessageService;
import com.bondarenko.pojo.MessagePojo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;

public class Consumer implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);
    private static final double MILLIS_IN_SECOND = 1000;
    private final MessageService messageService;
    private final String poisonPillMessage;
    private final CsvFileWriter fileWriter;
    private final int numberOfProducers;
    private final AtomicInteger globalMessageCounter;

    public Consumer(MessageService messageService,
                    String poisonPillMessage,
                    CsvFileWriter fileWriter,
                    int numberOfProducers,
                    AtomicInteger messageCounter) {
        this.messageService = messageService;
        this.poisonPillMessage = poisonPillMessage;
        this.fileWriter = fileWriter;
        this.numberOfProducers = numberOfProducers;
        this.globalMessageCounter = messageCounter;
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        LOGGER.info("Consumer {} start", name);
        long startMillis = System.currentTimeMillis();
        int messageCounter = 0;
        MessagePojo message;
        int poisonPillCounter = 0;
        while (poisonPillCounter < numberOfProducers) {
            message = messageService.pollMessage();
            if (poisonPillMessage.equals(message.getStatus())) {
                poisonPillCounter++;
            } else {
                fileWriter.addMessage(message);
                messageCounter++;
                globalMessageCounter.incrementAndGet();
            }
        }
        double passedSeconds = (System.currentTimeMillis() - startMillis) / MILLIS_IN_SECOND;
        String speed = String.format("%.2f", messageCounter / passedSeconds);
        LOGGER.info("Consumer {} stop, processed {} messages. Speed is {} messages per second", name, messageCounter, speed);
        messageService.close();
    }
}
