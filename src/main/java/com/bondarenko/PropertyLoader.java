package com.bondarenko;

import com.bondarenko.exception.PropertyFileDoesNotExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class PropertyLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyLoader.class);
    private static final Charset PROPERTY_ENCODING = StandardCharsets.UTF_8;
    private static final String PROPERTY_NUMBER_NAME = "N";
    private static final int DEFAULT_NUMBER = 1001;
    private static final int MIN_NUMBER_EXCLUDED = 1000;
    private static final String PROPERTY_FILE_NAME = "fileName";
    private static final String DEFAULT_FILE_NAME = "file";


    public Properties getProperties(String fileName) throws PropertyFileDoesNotExistsException {
        Properties properties = new Properties();

        File jarFile = new File(PropertyLoader.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        File propertyFile = new File(jarFile.getParent(), fileName);

        try {
            if (propertyFile.exists()) {
                try (FileReader propertyFileReader = new FileReader(propertyFile, PROPERTY_ENCODING)) {
                    properties.load(propertyFileReader);
                }
                LOGGER.debug("External property file loaded - {}", propertyFile.getAbsolutePath());
            } else {
                ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
                InputStream resourceAsStream = classLoader.getResourceAsStream(fileName);
                properties.load(new InputStreamReader(resourceAsStream, PROPERTY_ENCODING));
                LOGGER.debug("Internal property file loaded");
            }
        } catch (NullPointerException exception) {
            LOGGER.warn("property file not found", exception);
            throw new PropertyFileDoesNotExistsException(exception);
        } catch (IOException exception) {
            LOGGER.warn("property file can not be read", exception);
            throw new PropertyFileDoesNotExistsException(exception);
        }

        return properties;
    }

    public int getNumber() {
        String numberString = System.getProperty(PROPERTY_NUMBER_NAME);

        if (numberString == null) {
            LOGGER.warn("there is no \"{}\" property, used {}", PROPERTY_NUMBER_NAME, DEFAULT_NUMBER);
            return DEFAULT_NUMBER;
        }

        int number;
        try {
            number = Integer.parseInt(numberString);
        } catch (NumberFormatException exception) {
            LOGGER.warn("{} value is incorrect = {}, used {}",
                    PROPERTY_NUMBER_NAME, numberString, DEFAULT_NUMBER, exception);
            return DEFAULT_NUMBER;
        }

        if (number < MIN_NUMBER_EXCLUDED) {
            LOGGER.warn("value of \"{}\" property should be more than {}, used {} type",
                    PROPERTY_NUMBER_NAME, MIN_NUMBER_EXCLUDED, DEFAULT_NUMBER);
            return DEFAULT_NUMBER;
        }

        LOGGER.debug("property \"{}\" exists = {}", PROPERTY_NUMBER_NAME, number);
        return number;
    }

    public String getFileName() {
        String fileName = System.getProperty(PROPERTY_FILE_NAME);
        if (fileName == null) {
            LOGGER.warn("Used default file name = {}", DEFAULT_FILE_NAME);
            return DEFAULT_FILE_NAME;
        } else {
            LOGGER.debug("Property {} exists = {}", PROPERTY_FILE_NAME, fileName);
            return fileName;
        }
    }
}
