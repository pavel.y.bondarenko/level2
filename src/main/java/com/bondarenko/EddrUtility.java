package com.bondarenko;

public class EddrUtility {
    private static final String NUMBER_TEMPLATE = "\\d+";
    private static final int DECIMAL_RADIX = 10;
    private static final int FIRST_EDDR_MULTIPLIER = 7;
    private static final int SECOND_EDDR_MULTIPLIER = 3;

    private EddrUtility() {
    }

    public static int calculateCheckSum(String number) {
        if (number == null || !number.matches(NUMBER_TEMPLATE)) {
            return 0;
        }

        int checkSum = 0;
        for (int i = 0; i < number.length(); i++) {
            if ((i % 3) == 0) {
                checkSum += Character.digit(number.charAt(i), DECIMAL_RADIX) * FIRST_EDDR_MULTIPLIER;
            } else if ((i % 3) == 1) {
                checkSum += Character.digit(number.charAt(i), DECIMAL_RADIX) * SECOND_EDDR_MULTIPLIER;
            } else {
                checkSum += Character.digit(number.charAt(i), DECIMAL_RADIX);
            }
        }
        return checkSum;
    }
}
