package com.bondarenko;

import com.bondarenko.pojo.MessagePojo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Random;

public class Generator {
    private static final Random RANDOM = new Random();
    private static final int NAME_MIN_LENGTH_INCLUDED = 2;
    private static final int NAME_MAX_LENGTH_INCLUDED = 9;
    private static final int MIN_COUNT_INCLUDED = 1;
    private static final int MAX_COUNT_EXCLUDED = 31;
    private static final int START_YEAR_CREATE_AT = 2000;
    private static final int START_MONTH_CREATE_AT = 1;
    private static final int START_DAY_CREATE_AT = 1;
    private static final int START_YEAR_EDDR = 1950;
    private static final int FIRST_MONTH_NUMBER_INCLUSIVE = 1;
    private static final int LAST_MONTH_NUMBER_EXCLUSIVE = 13;
    private static final int FIRST_DAY_NUMBER_INCLUSIVE = 1;
    private static final int LAST_DAY_NUMBER_EXCLUSIVE = 32;
    private static final int DECIMAL_RADIX = 10;
    private static final int EDDR_DELIMITER_INDEX = 8;
    private static final String CONSONANT_LETTERS = "bcdfghjklmnpqrstvwxz";
    private static final String VOWEL_LETTERS = "aeiouy";

    public MessagePojo generateRandomMessagePojo() {
        return new MessagePojo(generateRandomName(),
                generateRandomEddr(),
                generateRandomCount(),
                generateRandomLocalDateTime());
    }

    private int generateRandomCount() {
        return RANDOM.ints(MIN_COUNT_INCLUDED, MAX_COUNT_EXCLUDED).findFirst().getAsInt();
    }

    private LocalDateTime generateRandomLocalDateTime() {
        return LocalDateTime.of(generateRandomLocalDate(), generateRandomLocalTime());
    }

    private LocalDate generateRandomLocalDate() {
        long startEpochDay = LocalDate.of(START_YEAR_CREATE_AT, START_MONTH_CREATE_AT, START_DAY_CREATE_AT).toEpochDay();
        long endEpochDay = LocalDate.now().toEpochDay();
        return LocalDate.ofEpochDay(RANDOM.longs(startEpochDay, endEpochDay).findFirst().getAsLong());
    }

    private LocalTime generateRandomLocalTime() {
        long startNanoOfDay = LocalTime.MIN.toNanoOfDay();
        long endNanoOfDay = LocalTime.MAX.toNanoOfDay();
        return LocalTime.ofNanoOfDay(RANDOM.longs(startNanoOfDay, endNanoOfDay).findFirst().getAsLong());
    }

    private String generateRandomEddr() {
        StringBuilder eddrBuilder = new StringBuilder();
        eddrBuilder.append(RANDOM.ints(START_YEAR_EDDR, LocalDate.now().getYear()).findFirst().getAsInt());

        int month = RANDOM.ints(FIRST_MONTH_NUMBER_INCLUSIVE, LAST_MONTH_NUMBER_EXCLUSIVE).findFirst().getAsInt();
        if (month < DECIMAL_RADIX) {
            eddrBuilder.append(0);
        }
        eddrBuilder.append(month);

        int day = RANDOM.ints(FIRST_DAY_NUMBER_INCLUSIVE, LAST_DAY_NUMBER_EXCLUSIVE).findFirst().getAsInt();
        if (day < DECIMAL_RADIX) {
            eddrBuilder.append(0);
        }
        eddrBuilder.append(day);
        RANDOM.ints(0, DECIMAL_RADIX).limit(4).forEach(eddrBuilder::append);

        int checkNumber = DECIMAL_RADIX - EddrUtility.calculateCheckSum(eddrBuilder.toString()) % DECIMAL_RADIX;
        eddrBuilder.append(checkNumber < DECIMAL_RADIX ? checkNumber : 0);

        return eddrBuilder.insert(EDDR_DELIMITER_INDEX, "-").toString();
    }

    private String generateRandomName() {
        StringBuilder name = new StringBuilder();
        name.append(Character.toUpperCase(CONSONANT_LETTERS.charAt(RANDOM.nextInt(CONSONANT_LETTERS.length()))));
        int length = RANDOM.nextInt(NAME_MAX_LENGTH_INCLUDED - NAME_MIN_LENGTH_INCLUDED) + NAME_MIN_LENGTH_INCLUDED;
        for (int i = 1; i <= length; i++) {
            if ((i & 1) == 0) {
                name.append(CONSONANT_LETTERS.charAt(RANDOM.nextInt(CONSONANT_LETTERS.length())));
            } else {
                name.append(VOWEL_LETTERS.charAt(RANDOM.nextInt(VOWEL_LETTERS.length())));
            }
        }
        return name.toString();
    }
}
