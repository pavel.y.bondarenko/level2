package com.bondarenko.eddr;

import com.bondarenko.EddrUtility;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.DateTimeException;
import java.time.LocalDate;

public class EddrValidator implements ConstraintValidator<Eddr, String> {
    private static final String EDDR_REGEX = "\\d{8}-?\\d{5}";
    private static final int DECIMAL_RADIX = 10;
    private static final int YEAR_FIRST_INDEX_INCLUDE = 0;
    private static final int YEAR_LAST_INDEX_EXCLUDE = 4;
    private static final int MONTH_FIRST_INDEX_INCLUDE = 4;
    private static final int MONTH_LAST_INDEX_EXCLUDE = 6;
    private static final int DAY_FIRST_INDEX_INCLUDE = 6;
    private static final int DAY_LAST_INDEX_EXCLUDE = 8;
    private static final int CHECK_SUM_INDEX = 12;

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (!s.matches(EDDR_REGEX)) {
            return false;
        }

        s = s.replace("-", "");
        int year = Integer.parseInt(s.substring(YEAR_FIRST_INDEX_INCLUDE, YEAR_LAST_INDEX_EXCLUDE));
        int month = Integer.parseInt(s.substring(MONTH_FIRST_INDEX_INCLUDE, MONTH_LAST_INDEX_EXCLUDE));
        int day = Integer.parseInt(s.substring(DAY_FIRST_INDEX_INCLUDE, DAY_LAST_INDEX_EXCLUDE));
        try {
            LocalDate.of(year, month, day);
        } catch (DateTimeException exception) {
            return false;
        }

        int checkSum = EddrUtility.calculateCheckSum(s.substring(0, CHECK_SUM_INDEX))
                + Character.digit(s.charAt(CHECK_SUM_INDEX), DECIMAL_RADIX);
        return checkSum % DECIMAL_RADIX == 0;
    }
}
