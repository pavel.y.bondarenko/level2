package com.bondarenko.eddr;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EddrValidator.class)
public @interface Eddr {
    String message() default "Eddr is not correct";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
