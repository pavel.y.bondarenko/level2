package com.bondarenko;

import com.bondarenko.jms.MessageService;
import com.bondarenko.pojo.MessagePojo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class Producer implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(Producer.class);
    private static final long MILLIS_IN_SECOND = 1000;
    private static final Generator GENERATOR = new Generator();
    private final MessageService messageService;
    private final int numberOfMessages;
    private final AtomicInteger globalMessageCounter;
    private final int stopTimeInSeconds;
    private final MessagePojo poisonPillMessage;
    private final int numberOfConsumers;

    public Producer(MessageService messageService,
                    int numberOfMessages,
                    AtomicInteger messageCounter,
                    int stopTimeInSeconds,
                    String poisonPillMessage,
                    int numberOfConsumers) {
        this.messageService = messageService;
        this.numberOfMessages = numberOfMessages;
        this.globalMessageCounter = messageCounter;
        this.stopTimeInSeconds = stopTimeInSeconds;
        this.poisonPillMessage = new MessagePojo(poisonPillMessage);
        this.numberOfConsumers = numberOfConsumers;
    }

    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        LOGGER.info("Producer {} start", name);
        long startMillis = System.currentTimeMillis();
        long stopMillis = startMillis + (stopTimeInSeconds * MILLIS_IN_SECOND);
        final AtomicInteger messageCounter = new AtomicInteger(0);

        Stream.generate(GENERATOR::generateRandomMessagePojo)
                .limit(numberOfMessages)
                .takeWhile(messagePojo -> System.currentTimeMillis() < stopMillis)
                .takeWhile(messagePojo -> globalMessageCounter.getAndIncrement() < numberOfMessages)
                .forEach(messagePojo -> {
                    messageService.pushMessage(messagePojo);
                    messageCounter.incrementAndGet();
                });
        globalMessageCounter.decrementAndGet();

        long passedSeconds = (System.currentTimeMillis() - startMillis) / MILLIS_IN_SECOND;
        LOGGER.debug("Producer {} sent {} of {} messages", name, messageCounter, numberOfMessages);
        LOGGER.debug("Producer {} passed {} of {} seconds", name, passedSeconds, stopTimeInSeconds);

        Stream.generate(() -> poisonPillMessage).limit(numberOfConsumers).forEach(messageService::pushMessage);

        String speed = String.format("%.2f", (double) messageCounter.get() / passedSeconds);
        LOGGER.info("Producer {} stop, speed is {} messages per second", name, speed);
        messageService.close();
    }
}
