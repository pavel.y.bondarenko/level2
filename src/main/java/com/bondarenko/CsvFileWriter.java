package com.bondarenko;

import com.bondarenko.exception.FileAccessException;
import com.bondarenko.exception.JsonCreateException;
import com.bondarenko.pojo.ErrorsPojo;
import com.bondarenko.pojo.MessagePojo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

public class CsvFileWriter implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(CsvFileWriter.class);

    private enum ValidCsvHeaders {name, count}

    private enum InvalidCsvHeaders {name, count, errors}

    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final String ERROR_SUFFIX = "-error";
    private static final String FILE_TYPE = ".csv";
    private final String fileName;
    private final BlockingQueue<MessagePojo> messagePojoQueue = new LinkedBlockingQueue<>();
    private boolean active = true;


    public CsvFileWriter(String fileName) {
        this.fileName = fileName;
    }

    public void addMessage(MessagePojo messagePojo) {
        if (active) {
            this.messagePojoQueue.add(messagePojo);
        }
    }

    public void stop() {
        this.active = false;
    }

    @Override
    public void run() {
        File validFile = createFile(fileName + FILE_TYPE);
        File invalidFile = createFile(fileName + ERROR_SUFFIX + FILE_TYPE);

        CSVFormat validCsvFormat = CSVFormat.DEFAULT.builder()
                .setHeader(ValidCsvHeaders.class)
                .build();
        CSVFormat invalidCsvFormat = CSVFormat.DEFAULT.builder()
                .setHeader(InvalidCsvHeaders.class)
                .build();

        try (final FileWriter validFileWriter = new FileWriter(validFile, true);
             final FileWriter invalidFileWriter = new FileWriter(invalidFile, true);
             final CSVPrinter validPrinter = new CSVPrinter(validFileWriter, validCsvFormat);
             final CSVPrinter invalidPrinter = new CSVPrinter(invalidFileWriter, invalidCsvFormat);
             ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
            Validator validator = factory.getValidator();
            while (active || !messagePojoQueue.isEmpty()) {
                if (!messagePojoQueue.isEmpty()) {
                    MessagePojo messagePojo = messagePojoQueue.poll();
                    Set<ConstraintViolation<MessagePojo>> violations = validator.validate(messagePojo);
                    if (violations.isEmpty()) {
                        validPrinter.printRecord(messagePojo.getName(), messagePojo.getCount());
                    } else {
                        String errors = getErrors(violations);
                        invalidPrinter.printRecord(messagePojo.getName(), messagePojo.getCount(), errors);
                    }
                }
            }
        } catch (IOException exception) {
            LOGGER.error("Can not write to file");
            throw new FileAccessException(exception);
        }
    }

    private File createFile(String fileName) {
        File file = new File(fileName);

        if (file.exists()) {
            try {
                Files.delete(file.toPath());
                Files.createFile(file.toPath());
            } catch (IOException exception) {
                LOGGER.error("Can not create file {}", file.getAbsolutePath());
                throw new FileAccessException(exception);
            }
        }

        return file;
    }

    private String getErrors(Set<ConstraintViolation<MessagePojo>> violations) {
        List<String> errors = violations.stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());
        try {
            return MAPPER.writeValueAsString(new ErrorsPojo(errors));
        } catch (JsonProcessingException exception) {
            LOGGER.error("Can not create json with {}", errors);
            throw new JsonCreateException(exception);
        }
    }
}
