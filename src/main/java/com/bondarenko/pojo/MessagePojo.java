package com.bondarenko.pojo;

import com.bondarenko.eddr.Eddr;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.time.LocalDateTime;

public class MessagePojo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Size(min = 7, message = "Name length must be {min} or more")
    @Pattern(regexp = "\\w*a\\w*", message = "Name should contain 'a'")
    private final String name;

    @Eddr()
    private final String eddr;

    @Min(value = 10, message = "Count must be {value} or more")
    private final int count;
    private final LocalDateTime created_at;
    private final String status;

    public MessagePojo(String name,
                       String eddr,
                       int count,
                       LocalDateTime created_at) {
        this.name = name;
        this.eddr = eddr;
        this.count = count;
        this.created_at = created_at;
        this.status = "ok";
    }

    public MessagePojo(String status) {
        this.name = "";
        this.eddr = "";
        this.count = 0;
        this.created_at = LocalDateTime.now();
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getEddr() {
        return eddr;
    }

    public int getCount() {
        return count;
    }

    public LocalDateTime getCreated_at() {
        return created_at;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "MessagePojo{" +
                "name='" + name + '\'' +
                ", eddr='" + eddr + '\'' +
                ", count=" + count +
                ", created_at=" + created_at +
                ", status='" + status + '\'' +
                '}';
    }
}
