package com.bondarenko.pojo;

import java.util.List;

public class ErrorsPojo {
    private final List<String> errors;

    public ErrorsPojo(List<String> errors) {
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }
}
