package com.bondarenko.exception;

public class PropertyFileDoesNotExistsException extends RuntimeException {
    public PropertyFileDoesNotExistsException(Exception exception) {
        super(exception);
    }
}
