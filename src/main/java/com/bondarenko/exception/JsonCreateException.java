package com.bondarenko.exception;

public class JsonCreateException extends RuntimeException {
    public JsonCreateException(Throwable cause) {
        super(cause);
    }
}
