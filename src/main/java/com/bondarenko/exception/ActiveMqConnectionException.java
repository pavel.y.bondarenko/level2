package com.bondarenko.exception;

public class ActiveMqConnectionException extends RuntimeException {
    public ActiveMqConnectionException(Throwable cause) {
        super(cause);
    }

    public ActiveMqConnectionException(String message) {
        super(message);
    }
}
