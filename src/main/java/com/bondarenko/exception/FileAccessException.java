package com.bondarenko.exception;

public class FileAccessException extends RuntimeException {

    public FileAccessException(Throwable exception) {
        super(exception);
    }
}
