package com.bondarenko.jms;

import com.bondarenko.exception.ActiveMqConnectionException;
import com.bondarenko.pojo.MessagePojo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;

public class MessageService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageService.class);
    private final ActiveMqSessionFactory sessionFactory;
    private Session session;
    private final String queueName;
    private MessageProducer producer;
    private MessageConsumer consumer;

    public MessageService(ActiveMqSessionFactory sessionFactory,
                          String queueName) {
        if (sessionFactory == null || queueName == null) {
            LOGGER.error("Session and queueName can not be null");
            throw new ActiveMqConnectionException("Session and queueName can not be null");
        }
        this.sessionFactory = sessionFactory;
        this.queueName = queueName;
    }

    public void pushMessage(MessagePojo messageObject) {
        try {
            if (producer == null) {
                session = sessionFactory.getSession();
                producer = session.createProducer(session.createQueue(queueName));
                producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            }
            ObjectMessage newMessage = session.createObjectMessage(messageObject);
            producer.send(newMessage);
        } catch (JMSException exception) {
            LOGGER.error("Can not push message to {} queue", queueName);
            throw new ActiveMqConnectionException(exception);
        }
    }

    public MessagePojo pollMessage() {
        try {
            if (consumer == null) {
                session = sessionFactory.getSession();
                consumer = session.createConsumer(session.createQueue(queueName));
            }
            ObjectMessage objectMessage = (ObjectMessage) consumer.receive();
            return (MessagePojo) objectMessage.getObject();
        } catch (JMSException exception) {
            LOGGER.error("Can not poll message from {} queue", queueName);
            throw new ActiveMqConnectionException(exception);
        } catch (ClassCastException exception) {
            LOGGER.error("Object class is incorrect");
            throw exception;
        }
    }

    public void close() {
        try {
            this.session.close();
        } catch (JMSException e) {
            LOGGER.warn("Can not close session");
        }
        this.sessionFactory.closeSession();
        this.session = null;
        this.consumer = null;
        this.producer = null;
    }
}
