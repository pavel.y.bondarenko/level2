package com.bondarenko.jms;

import com.bondarenko.exception.ActiveMqConnectionException;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Session;

import static javax.jms.Session.AUTO_ACKNOWLEDGE;

public class ActiveMqSessionFactory {
    private final ActiveMQConnectionFactory factory;
    private Connection connection;
    private boolean isClosed;

    private static final Logger LOGGER = LoggerFactory.getLogger(ActiveMqSessionFactory.class);

    public ActiveMqSessionFactory(String brokerUrl,
                                  String brokerUsername,
                                  String brokerPassword) {
        this.factory = new ActiveMQConnectionFactory(brokerUrl);
        this.factory.setUserName(brokerUsername);
        this.factory.setPassword(brokerPassword);
    }

    public Session getSession() {
        try {
            if (connection == null || isClosed) {
                connection = factory.createConnection();
                connection.start();
                isClosed = false;
                LOGGER.info("Connection created successfully");
            }
            return connection.createSession(false, AUTO_ACKNOWLEDGE);
        } catch (JMSException exception) {
            LOGGER.error("Can not connect to ActiveMQ, check properties");
            throw new ActiveMqConnectionException(exception);
        }
    }

    public void closeSession() {
        try {
            if (connection != null) {
                connection.close();
                isClosed = true;
                LOGGER.info("Connection closed");
            }
        } catch (JMSException exception) {
            LOGGER.error("Can not close session");
            throw new ActiveMqConnectionException(exception);
        }
    }
}
