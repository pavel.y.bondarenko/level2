package com.bondarenko;

import com.bondarenko.jms.ActiveMqSessionFactory;
import com.bondarenko.jms.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);
    private static final String BROKER_PROPERTY = "brokerUrl";
    private static final String QUEUE_PROPERTY = "queueName";
    private static final String STOP_TIME_PROPERTY = "stopTimeInSeconds";
    private static final String BROKER_USERNAME_PROPERTY = "brokerUsername";
    private static final String BROKER_PASSWORD_PROPERTY = "brokerPassword";
    private static final String PROPERTIES_FILE_NAME = "app.properties";
    private static final String PROPERTY_MISSED_TEMPLATE = "Property \"{}\" is missed";
    private static final String POISON_PILL_MESSAGE = "END";
    private static final int NUMBER_OF_PRODUCERS = 4;
    private static final int NUMBER_OF_CONSUMERS = 2;
    private static final double MILLIS_IN_SECOND = 1000;
    private static final long ADDITION_WAIT_TIME_IN_SECONDS = 10;

    public static void main(String[] args) {
        App app = new App();
        PropertyLoader propertyLoader = new PropertyLoader();

        Properties properties = propertyLoader.getProperties(PROPERTIES_FILE_NAME);
        if (app.isSomePropertyMissed(properties)) {
            return;
        }

        int stopTime;
        try {
            stopTime = app.getStopTimeFromProperties(properties);
        } catch (NumberFormatException exception) {
            LOGGER.error("Program stopped", exception);
            return;
        }

        AtomicInteger producedMessageCounter = new AtomicInteger();
        AtomicInteger consumedMessageCounter = new AtomicInteger();

        CsvFileWriter writer = new CsvFileWriter(propertyLoader.getFileName());
        Thread fileWriter = new Thread(writer);
        fileWriter.start();

        long startMillis = System.currentTimeMillis();

        ExecutorService producersExecutor = app.startProducers(properties,
                                                               propertyLoader.getNumber(),
                                                               producedMessageCounter,
                                                               stopTime);

        ExecutorService consumersExecutor = app.startConsumers(properties, writer, consumedMessageCounter);

        producersExecutor.shutdown();
        try {
            if (!producersExecutor.awaitTermination(stopTime + ADDITION_WAIT_TIME_IN_SECONDS, TimeUnit.SECONDS)) {
                producersExecutor.shutdownNow();
            }
        } catch (InterruptedException e) {
            producersExecutor.shutdownNow();
        }

        double producersWorkTimeInSeconds = (System.currentTimeMillis() - startMillis) / MILLIS_IN_SECOND;
        LOGGER.info("All {} producers work {} seconds", NUMBER_OF_PRODUCERS, producersWorkTimeInSeconds);
        LOGGER.info("All {} producers sent {} messages", NUMBER_OF_PRODUCERS, producedMessageCounter.get());
        LOGGER.info("Average speed of producers is {}", producedMessageCounter.get() / producersWorkTimeInSeconds);

        consumersExecutor.shutdown();
        try {
            if (!consumersExecutor.awaitTermination(stopTime + ADDITION_WAIT_TIME_IN_SECONDS, TimeUnit.SECONDS)) {
                consumersExecutor.shutdownNow();
            }
        } catch (InterruptedException e) {
            consumersExecutor.shutdownNow();
        }

        double consumersWorkTimeInSeconds = (System.currentTimeMillis() - startMillis) / MILLIS_IN_SECOND;
        LOGGER.info("All {} consumers work {} seconds", NUMBER_OF_CONSUMERS, consumersWorkTimeInSeconds);
        LOGGER.info("All {} consumers get {} messages", NUMBER_OF_CONSUMERS, consumedMessageCounter.get());
        LOGGER.info("Average speed of consumers is {}", consumedMessageCounter.get() / consumersWorkTimeInSeconds);

        writer.stop();
        try {
            fileWriter.join();
        } catch (InterruptedException exception) {
            LOGGER.error("Writer stopped with exception", exception);
            Thread.currentThread().interrupt();
        }
    }

    private ExecutorService startProducers(Properties properties,
                                           int maxNumberOfMessages,
                                           AtomicInteger messageCounter,
                                           int stopTimeInSeconds) {
        ExecutorService executor = Executors.newFixedThreadPool(NUMBER_OF_PRODUCERS);
        Stream.generate(() -> new Producer(createNewMessageService(properties),
                                           maxNumberOfMessages,
                                           messageCounter,
                                           stopTimeInSeconds,
                                           POISON_PILL_MESSAGE,
                                           NUMBER_OF_CONSUMERS))
              .limit(NUMBER_OF_PRODUCERS)
              .forEach(executor::execute);
        return executor;
    }

    private ExecutorService startConsumers(Properties properties,
                                           CsvFileWriter writer,
                                           AtomicInteger consumedMessageCounter) {
        ExecutorService executor = Executors.newFixedThreadPool(NUMBER_OF_CONSUMERS);
        Stream.generate(() -> new Consumer(createNewMessageService(properties),
                                           POISON_PILL_MESSAGE,
                                           writer,
                                           NUMBER_OF_PRODUCERS,
                                           consumedMessageCounter))
              .limit(NUMBER_OF_CONSUMERS)
              .forEach(executor::execute);
        return executor;
    }

    private MessageService createNewMessageService(Properties properties) {
        ActiveMqSessionFactory factory = new ActiveMqSessionFactory(properties.getProperty(BROKER_PROPERTY),
                                                                    properties.getProperty(BROKER_USERNAME_PROPERTY),
                                                                    properties.getProperty(BROKER_PASSWORD_PROPERTY));
        return new MessageService(factory, properties.getProperty(QUEUE_PROPERTY));
    }

    public int getStopTimeFromProperties(Properties properties) {
        String stopTimeString = properties.getProperty(STOP_TIME_PROPERTY);
        try {
            return Integer.parseInt(stopTimeString);
        } catch (NumberFormatException exception) {
            LOGGER.error("{} property format is incorrect = {}", STOP_TIME_PROPERTY, stopTimeString);
            throw exception;
        }
    }

    public boolean isSomePropertyMissed(Properties properties) {
        boolean notValid = false;

        if (!properties.containsKey(BROKER_PROPERTY)) {
            LOGGER.error(PROPERTY_MISSED_TEMPLATE, BROKER_PROPERTY);
            notValid = true;
        }

        if (!properties.containsKey(QUEUE_PROPERTY)) {
            LOGGER.error(PROPERTY_MISSED_TEMPLATE, QUEUE_PROPERTY);
            notValid = true;
        }

        if (!properties.containsKey(STOP_TIME_PROPERTY)) {
            LOGGER.error(PROPERTY_MISSED_TEMPLATE, STOP_TIME_PROPERTY);
            notValid = true;
        }

        return notValid;
    }
}
