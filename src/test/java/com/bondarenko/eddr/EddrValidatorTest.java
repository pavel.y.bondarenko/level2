package com.bondarenko.eddr;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EddrValidatorTest {

    private EddrValidator eddrValidator;

    @BeforeEach
    void setUp() {
        eddrValidator = new EddrValidator();
    }

    @AfterEach
    void tearDown() {
        eddrValidator = null;
    }

    @Test
    void validEddrTest() {
        assertTrue(eddrValidator.isValid("20141115-75823", null));
    }

    @Test
    void invalidCheckSumEddrTest() {
        assertFalse(eddrValidator.isValid("20040217-86201", null));
    }

    @Test
    void invalidParamEddrTest() {
        assertFalse(eddrValidator.isValid("invalid value", null));
    }

    @Test
    void invalidDateEddrTest() {
        assertFalse(eddrValidator.isValid("99999999-99999", null));
    }
}