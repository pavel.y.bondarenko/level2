package com.bondarenko;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EddrUtilityTest {

    @Test
    void validCalculateCheckSumTest() {
        assertEquals(147, EddrUtility.calculateCheckSum("123456789"));
    }

    @Test
    void nullCalculateCheckSumTest() {
        assertEquals(0, EddrUtility.calculateCheckSum(null));
    }

    @Test
    void invalidValueCalculateCheckSumTest() {
        assertEquals(0, EddrUtility.calculateCheckSum("not a number"));
    }
}