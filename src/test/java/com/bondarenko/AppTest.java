package com.bondarenko;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    private App app;

    @BeforeEach
    void setUp() {
        app = new App();
    }

    @AfterEach
    void tearDown() {
        app = null;
    }

    @Test
    void missedPropertiesTest() {
        Properties properties = new Properties();
        assertTrue(app.isSomePropertyMissed(properties));
    }

    @Test
    void allPropertiesTest() {
        Properties properties = new Properties();
        properties.put("brokerUrl", "");
        properties.put("queueName", "");
        properties.put("stopTimeInSeconds", "");
        assertFalse(app.isSomePropertyMissed(properties));
    }

    @Test
    void validStopTimeFromPropertiesTest() {
        Properties properties = new Properties();
        properties.put("stopTimeInSeconds", "123");
        assertEquals(123, app.getStopTimeFromProperties(properties));
    }

    @Test
    void invalidStopTimeFromPropertiesThrowExceptionTest() {
        Properties properties = new Properties();
        properties.put("stopTimeInSeconds", "12ab3");
        assertThrows(NumberFormatException.class, () -> app.getStopTimeFromProperties(properties));
    }
}