package com.bondarenko;

import com.bondarenko.exception.PropertyFileDoesNotExistsException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class PropertyLoaderTest {
    private PropertyLoader propertyLoader;

    @BeforeEach
    void setUp() {
        propertyLoader = new PropertyLoader();
    }

    @AfterEach
    void tearDown() {
        propertyLoader = null;
    }

    @Test
    void getNotExistsPropertiesTest() {
        assertThrows(PropertyFileDoesNotExistsException.class,
                () -> propertyLoader.getProperties("unknown.properties"));
    }

    @Test
    void getExistsPropertiesTest() {
        Properties properties = propertyLoader.getProperties("test.properties");
        assertTrue(properties.containsKey("test"));
        assertTrue(properties.containsKey("property"));
        assertEquals("12", properties.getProperty("test"));
        assertEquals("string", properties.getProperty("property"));
    }

    @Test
    void getCorrectNumberTest() {
        System.setProperty("N", "1002");
        assertEquals(1002, propertyLoader.getNumber());
    }

    @Test
    void getLowerNumberTest() {
        System.setProperty("N", "500");
        assertEquals(1001, propertyLoader.getNumber());
    }

    @Test
    void getIncorrectNumberTest() {
        System.setProperty("N", "13.5");
        assertEquals(1001, propertyLoader.getNumber());
    }

    @Test
    void getDefaultNumberTest() {
        System.clearProperty("N");
        assertEquals(1001, propertyLoader.getNumber());
    }

    @Test
    void getExistedFileNameTest() {
        System.setProperty("fileName", "name");
        assertEquals("name", propertyLoader.getFileName());
    }

    @Test
    void getDefaultFileName() {
        assertEquals("file", propertyLoader.getFileName());
    }
}