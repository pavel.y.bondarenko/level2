package com.bondarenko;

import com.bondarenko.pojo.MessagePojo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class GeneratorTest {
    private static final String NAME_TEMPLATE = "[A-Z]{1}[a-z]+";
    private static final String EDDR_PATTERN = "\\d{8}-\\d{5}";
    private static final LocalDateTime START_DATE_TIME = LocalDateTime.of(2000, 1, 1, 0, 0).minusMinutes(1);
    private static final LocalDateTime END_DATE_TIME = LocalDateTime.now();
    private Generator generator;

    @BeforeEach
    void setUp() {
        generator = new Generator();
    }

    @AfterEach
    void tearDown() {
        generator = null;
    }

    @RepeatedTest(100)
    void messagePojoGeneratorNameTest() {
        MessagePojo pojo = generator.generateRandomMessagePojo();
        assertNotNull(pojo);
        String name = pojo.getName();
        assertNotNull(name);
        assertTrue(name.length() > 1);
        assertTrue(name.length() < 10);
        assertTrue(name.matches(NAME_TEMPLATE));
    }

    @RepeatedTest(100)
    void messagePojoGeneratorEddrTest() {
        MessagePojo pojo = generator.generateRandomMessagePojo();
        assertNotNull(pojo);
        String eddr = pojo.getEddr();
        assertNotNull(eddr);
        assertTrue(eddr.matches(EDDR_PATTERN));
    }

    @RepeatedTest(100)
    void messagePojoGeneratorCountTest() {
        MessagePojo pojo = generator.generateRandomMessagePojo();
        assertNotNull(pojo);
        int count = pojo.getCount();
        assertTrue(count > 0);
        assertTrue(count < 31);
    }

    @RepeatedTest(100)
    void messagePojoGeneratorCreatedAtTest() {
        MessagePojo pojo = generator.generateRandomMessagePojo();
        assertNotNull(pojo);
        LocalDateTime dateTime = pojo.getCreated_at();
        assertTrue(dateTime.isAfter(START_DATE_TIME));
        assertTrue(dateTime.isBefore(END_DATE_TIME));
    }
}